import React from 'react'

const Square = (props) => {
        const design = props.value ? (props.value == 'X' ?  "square btn btn-primary" : "square btn btn-dark" ) : "square btn btn-warning" ;
        return (
            <button className={design} onClick = {props.onClick}>
                {props.value}
            </button>
        )
}

export default Square;
